// Address of the Whitelist Contract that you deployed
const WHITELIST_CONTRACT_ADDRESS = "0xb30bc4a077213619B20a081a79829CD6240f858c";
// URL to extract Metadata for a Crypto Dev NFT
const METADATA_URL = "https://learn-web3-nft-collection.vercel.app/api/";

module.exports = { WHITELIST_CONTRACT_ADDRESS, METADATA_URL };